package edu.olivet.se530.dummy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import edu.olivet.se530.HtmlCrawler;

public class DummyHtmlCrawler implements HtmlCrawler {

	@Override
	public Document getDocument(String isbn, String condition) throws IOException {
		/* Modified: Override implementation */
		String fileName = getFilePath(isbn, condition);
		File file = new File(getClass().getResource(fileName).getFile());
		if (file.isFile())
			return Jsoup.parse(file, "UTF-8");

		throw new FileNotFoundException();
	}
	
	private String getFilePath(String isbn, String condition) {
		String paddedIsbn = StringUtils.leftPad(isbn, 10, '0');
		String cookedCondition = condition.replaceAll(" ", "").replaceAll("-", "_");
		String fileName = "/" + paddedIsbn + "_" + cookedCondition.toUpperCase() + "_1.html";
		return fileName;
	}

}
